import 'package:flutter/material.dart';

class AddTodo extends StatelessWidget {
  final TextEditingController _addNameController = TextEditingController();
  final TextEditingController _addDescriptionController =
      TextEditingController();
  final Function addTodo;
  final riskOptions = ['Low', 'Medium', 'Critical'];
  final String dropdownValue = 'Low';

  AddTodo({@required this.addTodo});

  void _handleSubmit() {
    print(_addNameController.text);
    addTodo({
      'label': _addNameController.text,
      'description': _addDescriptionController.text
    });
    _addNameController.text = '';
    _addDescriptionController.text = '';
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text(
              'Add new todo!',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: _addNameController,
              decoration: InputDecoration(
                hintText: 'Name',
                labelText: 'Name',
                labelStyle: TextStyle(fontSize: 16),
                hintStyle: TextStyle(fontSize: 12),
                border: OutlineInputBorder(),
                suffixIcon: Icon(Icons.title),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: _addDescriptionController,
              decoration: InputDecoration(
                hintText: 'Description',
                labelText: 'Description',
                labelStyle: TextStyle(fontSize: 16),
                hintStyle: TextStyle(fontSize: 12),
                border: OutlineInputBorder(),
                suffixIcon: Icon(Icons.description),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: _handleSubmit,
              child: Text('Add'),
            ),
          )
        ],
      ),
    );
  }
}
