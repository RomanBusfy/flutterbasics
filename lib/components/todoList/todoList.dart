import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class TodoList extends StatelessWidget {
  final List<Map<String, String>> todoItems;
  final Function removeItem;
  final Function openInfo;

  TodoList(
      {@required this.todoItems,
      @required this.removeItem,
      @required this.openInfo});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ...todoItems.asMap().keys.toList().map((index) => Container(
              margin: const EdgeInsets.all(8.0),
              child: Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                child: Container(
                  color: Colors.white,
                  child: ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.indigoAccent,
                      child: Text('$index'),
                      foregroundColor: Colors.white,
                    ),
                    title: Text(todoItems[index]['label']),
                    subtitle: Text(todoItems[index]['description']),
                  ),
                ),
                secondaryActions: <Widget>[
                  IconSlideAction(
                    caption: 'Info',
                    color: Colors.blue,
                    icon: Icons.info,
                    onTap: () => openInfo(todoItems[index]),
                  ),
                  IconSlideAction(
                    caption: 'Delete',
                    color: Colors.red,
                    icon: Icons.delete,
                    onTap: () => removeItem(index),
                  ),
                ],
              ),
            ))
      ],
    );
  }
}
