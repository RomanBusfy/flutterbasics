import 'package:flutter/material.dart';

import '../components/todoList/todoList.dart';
import '../components/todoList/addDrawer.dart';
import '../components/todoList/infoDrawer.dart';

class TodoListScreen extends StatefulWidget {
  @override
  _TodoListState createState() => _TodoListState();
}

class _TodoListState extends State<TodoListScreen> {
  // create special key to add it into Scaffold and be able to pick it anytime
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  List<Map<String, String>> _todoItems = [];
  Map<String, String> _openedDetail = {};

  void _addTodo(Map<String, String> newTodo) {
    Navigator.pop(_scaffoldKey.currentContext);
    setState(() {
      _todoItems.add(newTodo);
    });
    _showSnackbar(newTodo['label']);
  }

  void _showSnackbar(String newTodo) {
    final snackBar = SnackBar(
      content: Text('$newTodo was added into list!'),
      backgroundColor: Colors.green,
      behavior: SnackBarBehavior.floating,
      duration: Duration(seconds: 1),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void _handleRemove(int index) {
    final snackBar = SnackBar(
      content: Text('Item was removed!'),
      backgroundColor: Colors.orange,
      behavior: SnackBarBehavior.floating,
      duration: Duration(seconds: 1),
    );
    setState(() {
      _todoItems.removeAt(index);
    });
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void _handleOpenInfo(Map<String, String> item) {
    _scaffoldKey.currentState.openEndDrawer();
    setState(() {
      _openedDetail = item;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Center(
        child: TodoList(
          todoItems: _todoItems,
          removeItem: _handleRemove,
          openInfo: _handleOpenInfo,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 50.0,
        ),
      ),
      floatingActionButton: Builder(
          builder: (BuildContext innerContext) => FloatingActionButton(
                onPressed: Scaffold.of(innerContext).openDrawer,
                tooltip: 'Increment Counter',
                child: Icon(Icons.add),
              )),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      endDrawer: InfoDrawer(item: _openedDetail),
      endDrawerEnableOpenDragGesture: false,
      drawer: AddTodo(
        addTodo: _addTodo,
      ),
    );
  }
}
